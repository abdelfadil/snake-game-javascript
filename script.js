var level = prompt("Entre le niveau de difficulte (facile, moyen, difficile) : ");

window.onload = function(){
/*
declaration des variables globales
*/

	var canvasWidth = 600;
	var canvasHeight = 400;
	var blockSize = 10;
	var ctx;
	var delay;
	if(level == "1"){
		delay = 300;
	}else{
		if(level == "2"){
			delay = 150;
		}else{
			delay = 90;
		}
	}
	//var delay = 150;
	var snakee;
	var applee;
	var widthInBlocks = canvasWidth/blockSize;
	var heightInBlocks = canvasHeight/blockSize;
	var score;
	var timeout;
/*
init est le coeur de l'execution du programme
*/
	init();
/*
cette fonction appele les autres fonction 
afin de les executer
*/
	function init(){
		var canvas = document.createElement('canvas');
		canvas.width = canvasWidth;
		canvas.height = canvasHeight;
		canvas.style.border = "20px solid blue";//CSS appliqué autour du canvas
		canvas.style.margin = "100px auto";
		canvas.style.display = "block";
		canvas.style.backgroundColor = "gray";
		document.body.appendChild(canvas);
		ctx = canvas.getContext('2d');
		snakee = new Snake([[4,2], [3,2], [2,2],[1,2]],"right");
		applee = new Apple([18,10]);
		score = 0;
		refreshCanvas();
	}
/*
cette fonction permet de raffrechire notre canvas lorsque
le delay est parvenu
*/
	function refreshCanvas(){
		snakee.advance();

		if(snakee.checkCollision()){
			gameOver();
		} else {
			if(snakee.isEatingApple(applee)){
				score++;
				snakee.ateApple = true;
				do{
					applee.setNewPosition();
				} while(applee.isOnsnake(snakee))
			}
			ctx.clearRect(0,0,canvasWidth, canvasHeight);
			drawscore();
			snakee.draw();
			applee.draw();
			timeout = setTimeout(refreshCanvas, delay);
		}
	}
/*
cette fonction est celle qui nous montre que nous 
avons perdu une partie du jeu
*/
	function gameOver(argument) {
		ctx.save();
		ctx.font = "bold 50px sans-serif";
		ctx.fillStyle = "#000";
		ctx.textAlign = "center";
		ctx.textBaseline = "middle";
		ctx.strokeStyle = "white";
		ctx.lineWidth = 5;
		var positionGameOverX = canvasWidth/2;
		var positionGameOverY = canvasHeight/2;
		ctx.strokeText("Game Over!", positionGameOverX, positionGameOverY - 120);
		ctx.fillText("Game Over!", positionGameOverX, positionGameOverY - 120);
		ctx.font = "bold 20px sans-serif";
		ctx.strokeText("Appuyer sur la touche Espace pour rejouer", positionGameOverX, positionGameOverY - 60);
		ctx.fillText("Appuyer sur la touche Espace pour rejouer", positionGameOverX, positionGameOverY - 60);
		ctx.restore();
	}
/*
cette fonction permet de redemarer le jeu lorsque le player
appuir sur la touche Espace
*/
	function restart(){
		snakee = new Snake([[6,4], [5,4], [4,4], [3,4], [2,4]],"right");
		applee = new Apple([10,10]);
		score = 0;
		clearTimeout(timeout);
		refreshCanvas();
	}
/*
cette fonction permet d'afficher permanament le score 
du player pour qu'il puise le voir
*/
	function drawscore(){
		ctx.save();
		ctx.font = "bold 60px sans-serif";
		ctx.fillStyle = "#000";
		ctx.fillText(score.toString(), 40, canvasHeight - 10);
		ctx.restore();
	}
/*
cette fonction permet de consevoir la pseudo-grille
imaginaire du canvas
*/
	function drawBlock(ctx, position){
		var x = position[0] * blockSize;
		var y = position[1] * blockSize;
		ctx.fillRect(x,y, blockSize, blockSize);
	}
/*
fonction constructrice du serpent
*/
	function Snake(body, direction){
		this.body = body; //propriete de son corps
		this.direction = direction //propriete de ses directioin
		this.ateApple = false; // propriete pour avoir mange une pomme
		this.draw = function(){ //methode permettant de dessiner le corps du serpent
			ctx.save();
			ctx.fillStyle = "lime";
			for (var i = 0; i< this.body.length; i++)
			{
				drawBlock(ctx, this.body[i]);
			}
			ctx.restore();
		};
		this.advance= function(){ //methode d'avancement
					var nexPosition = this.body[0].slice();
			switch(this.direction){
				case "left":
					nexPosition[0] -= 1;
					break;
				case "right":
					nexPosition[0] += 1;
					break;
				case "down":
					nexPosition[1] += 1;
					break;
				case "up":
					nexPosition[1] -= 1;
					break;
				default:
					throw("Invalid Direction");
			}
			this.body.unshift(nexPosition);
			if(!this.ateApple)
				this.body.pop();
			else
				this.ateApple = false;
		};
		this.setDirection = function(newDirection){ //methode de direction
					var allowedDirections;
			switch(this.direction){
				case "left":
				case "right":
					allowedDirections = ["up", "down"];
					break;
				case "down":
				case "up":
					allowedDirections = ["left", "right"];
					break;
				default:
					throw("Invalid Direction");
			}
			if (allowedDirections.indexOf(newDirection) > -1){
				this.direction = newDirection;
			}
		};
		this.checkCollision = function(){ //methode de verification collision
			var wallCollision = false;
			var SnakeCollision = false;
			var head = this.body[0];
			var rest = this.body.slice(1);
			var snakeX = head[0];
			var snakeY = head[1];
			var minX = 0;
			var minY = 0;
			var maxX = widthInBlocks - 1;
			var maxY = heightInBlocks - 1;
			var isNotBetweenHorizontalWalls = snakeX < minX || snakeX > maxX
			var isNotBetweenVerticalWalls = snakeY < minY || snakeY > maxY

			if (isNotBetweenHorizontalWalls || isNotBetweenVerticalWalls){
				wallCollision = true;
			}
			for (var i = 0; i < rest.length; i++){
				if(snakeX === rest[i][0] && snakeY === rest[i][1]){
					SnakeCollision = true;
				}
			}

			return wallCollision || SnakeCollision;

		};
		this.isEatingApple = function(appleToEat){ //methedo de verification si le serpent a bouffer la pomme
			var head = this.body[0];
			if(head[0] === appleToEat.position[0] && head[1] === appleToEat.position[1])
				return true;
			else
				return false;
		};
	}
/*
fonction constructrice de la pomme 
*/
	function Apple(position) {
		this.position = position; // propriete de position de la pomme
		this.draw = function(){ //methode permettant de dessiner le corps de la pomme
			ctx.save();
			ctx.fillStyle = "red";
			ctx.beginPath();
			var radius = blockSize/2;
			var x = this.position[0]*blockSize + radius;
			var y = this.position[1]*blockSize + radius;
			ctx.arc(x,y, radius, 0, Math.PI*2, true);
			ctx.fill();
			ctx.restore();
		};
		this.setNewPosition = function(){  // methode de position de la pomme
			var newX = Math.round(Math.random() * (widthInBlocks - 2));		
			var newY = Math.round(Math.random() * (heightInBlocks - 2));
			this.position = [newX, newY];
		};
		this.isOnsnake = function(snakeToCheck){ //methode pour verifier si la pomme est sur le serpent
			var isOnsnake = false;
			for (var i = 0; i < snakeToCheck.body.length; i++) {
				if(this.position[0] === snakeToCheck.body[i][0] && this.position[1] === snakeToCheck.body[i][1]){
					isOnsnake = true;
				}
			}
			return isOnsnake;
		};
	}
/*
cette fonction permet d'attache les directions entrée 
par le player au programme
*/
	document.onkeydown = function handleKeyDown(e) {
		var key = e.keyCode;
		var newDirection;
		switch(key){
			case 37:
				newDirection = "left";
				break;
			case 38:
				newDirection = "up";
				break;
			case 39:
				newDirection = "right";
				break;
			case 40:
				newDirection ="down";
				break;
			case 32:
				restart();
				return;
				break;
			default:
				return;
		}
		snakee.setDirection(newDirection);
	}

}